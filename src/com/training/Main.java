package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

import java.util.ArrayList;

public class Main {

    private String nameOfProgram;

    public static void main(String[] args) {

        // nameOfProgram = "xyz
        // cannot do above because this main can be accessed before an instance of the main class is created

        Pet myPet = new Pet();
        myPet.setNumLegs(4);
        myPet.setAge(1);
        myPet.feed();
        System.out.println(myPet.getNumLegs());
        System.out.println(myPet.getAge());

        Dragon myDragon = new Dragon();
        myDragon.setNumLegs(3);
        myDragon.setAge(100);
        myDragon.feed();
        System.out.println(myDragon.getNumLegs());
        System.out.println(myDragon.getAge());
        myDragon.breatheFire();

        Pet secondDragon = new Dragon();
        secondDragon.setNumLegs(12);
        secondDragon.setAge(100);
        secondDragon.feed();
        System.out.println(secondDragon.getNumLegs());
        System.out.println(secondDragon.getAge());
//        secondDragon.breatheFire();


        Pet[] petArray = new Pet[10];


        Pet firstPet = new Pet();
        firstPet.setName("Murph");

        petArray[0] = firstPet;

        // put a dragon in the array?
        Dragon myDrag = new Dragon("Spyro");
        petArray[1] = myDrag;
        // why is this okay? because com.training.pets.Pet IS A dragon
        // problem when we take it out again though
        // putting a dragon into an array of pets means we lost the extra dragon capabilities
        // limiting our view to just pet methods
        // cannot do petArray[1].feed("goats") which is a com.training.pets.Dragon method or .breatheFire()



        for(int i=0; i<petArray.length; ++i){
            System.out.println(petArray[i]);
        }

        ArrayList<Pet> myPetList = new ArrayList<Pet>();




    }
}
