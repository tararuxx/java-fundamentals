package com.training;

import com.training.pets.Dragon;

public class StringsMain {
    public static void main(String[] args) {
        String bigString = "start: ";

        StringBuilder stringBuilder = new StringBuilder(bigString);

        for(int i=0; i<10; ++i){
            bigString += i;
            stringBuilder.append(i);
        }

        System.out.println(bigString);
        System.out.println(stringBuilder);

        String testString = "hello" + " " + "lunch";

        Dragon stringyDragon = new Dragon("Smaug");
        // stringyDragon output below final num would be location in memory
        // not good string representation of the dragon
        System.out.println("Dragon created: " + stringyDragon);

        // we want a printout of its details that would be nice
        // extends Pet, Dragon is a pet, inherits these methods
        // Pet extends object, is an object, inherits these methods too
        System.out.println(stringyDragon.toString());
    }

}

