package com.training.pets;

import com.training.Feedable;

public class Dragon extends Pet implements Feedable {

    public static int numPets = 0;

    public Dragon() {
        this.setNumLegs(4);
    }

    public Dragon(String name) {
        numPets++;
        this.setNumLegs(4);
        this.setName(name);
    }

    public void feed() {
        System.out.println("Feed dragon some people");
    }

    public void feed(String food) {
        System.out.println("Feed dragon some " + food);
    }

    public void breatheFire() {
        System.out.println("FIREEEEEEE!!!");
    }

    public static int getNumPets(){
        // this.name = "Spyro";
        // cannot do above. At the time i create an instance, name etc are set
        // but this method can be accessed before any pets are created
        return numPets;
    }

    // overriding parent toString() method, the Object method
    @Override
    public String toString(){
        return "This is a dragon with " + this.getNumLegs() +
                " legs, called " + this.getName();
    }
}
